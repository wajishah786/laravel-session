<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\DegreeController;
use App\Http\Controllers\SubjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('degrees',[DegreeController::class,'index']);
// Route::get('degrees/create', [DegreeController::class, 'create']);
// Route::get('subjects', [SubjectController::class, 'index']);

Route::resource('books',BookController::class)->name('all','books');
// Route::resource('photos', PhotoController::class);

// Route::get('books', [BookController::class, 'index']);
// Route::get('books/create',[BookController::class,'create']);
// Route::post('books/store', [BookController::class, 'store']);


