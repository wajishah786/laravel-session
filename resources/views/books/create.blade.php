<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Vertical (basic) form</h2>
  <form method="POST" action="{{route('books.store')}}">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="email">Title:</label>
      <input type="text" class="form-control" placeholder="Enter email" name="title">
    </div>

    <div class="form-group">
      <label for="email">ISBN:</label>
      <input type="text" class="form-control" placeholder="Enter email" name="isbn">
    </div>
    <div class="form-group">
      <label for="email">Author:</label>
      <input type="text" class="form-control" placeholder="Enter email" name="author">
    </div>

    <div class="form-group">
      <label for="email">Edition:</label>
      <input type="text" class="form-control" placeholder="Enter email" name="edition">
    </div>


    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

</body>
</html>
