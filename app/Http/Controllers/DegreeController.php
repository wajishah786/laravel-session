<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Degree;

class DegreeController extends Controller
{
    public function index(){
        $degrees = Degree::all();

        foreach ($degrees as $degree) {
            echo $degree->id . " ". $degree->title;
            echo "<hr>";
        }

    }


    public function create(){
        // $abc = Degree::create(
        //     ['title' => 'Networking']
        // );

        $abc = new Degree();
        $abc->title = 'MCS';
        $abc->save();

        dd($abc);
    }
}
