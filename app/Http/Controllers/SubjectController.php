<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;

class SubjectController extends Controller
{
    public function index(){
        $degrees = Subject::all();

        foreach ($degrees as $degree) {
            echo $degree->id . " ". $degree->title;
            echo "<hr>";
        }

    }

}
